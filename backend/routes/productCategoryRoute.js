const express = require('express');

const productCategoryController = require('../controllers/productCategoryController');

const router = express.Router();

router
  .route('/')
  .get(productCategoryController.getAllProductCategories)
  .post(productCategoryController.addProductCategory);

router
  .route('/:id')
  .get(productCategoryController.getProductCategory)
  .delete(productCategoryController.deleteProductCategory)
  .patch(productCategoryController.updateProductCategory);

module.exports = router;
