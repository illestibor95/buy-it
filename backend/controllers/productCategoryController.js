const ProductCategory = require('../models/productCategoryModel');
const factory = require('./handlerFactory');

exports.getProductCategory = factory.getOne(ProductCategory);
exports.getAllProductCategories = factory.getAll(ProductCategory);
exports.updateProductCategory = factory.updateOne(ProductCategory);
exports.deleteProductCategory = factory.deleteOne(ProductCategory);
exports.addProductCategory = factory.createOne(ProductCategory);
