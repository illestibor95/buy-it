const Product = require('../models/productModel');
const factory = require('./handlerFactory');

exports.getProduct = factory.getOne(Product);
exports.getAllProducts = factory.getAll(Product);
exports.addProduct = factory.createOne(Product);
exports.updateProduct = factory.updateOne(Product);
exports.deleteProduct = factory.deleteOne(Product);
