const mongoose = require('mongoose');

const productCategoryScheme = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'A product category must have a name!'],
  },
  description: {
    type: String,
    required: [true, 'A product category must have a description!'],
  },
});

const ProductCategory = mongoose.model(
  'ProductCategory',
  productCategoryScheme
);

module.exports = ProductCategory;
