const mongoose = require('mongoose');
const validator = require('validator');

const productScheme = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Product must have a name!'],
    },
    description: {
      type: String,
      required: [true, 'Product must have a description!'],
    },
    quantity: {
      type: Number,
      min: [0, 'A product quanity must be at least zero!'],
    },
    imgPath: {
      type: String,
    },
    isDisabled: {
      type: Boolean,
      default: false,
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'ProductCategory',
      required: [true, 'Product must have a category!'],
    },
  },
  {
    //virtual fields are outputed
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

const Product = mongoose.model('Product', productScheme);

module.exports = Product;
